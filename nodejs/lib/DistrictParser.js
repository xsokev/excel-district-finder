var XLSX = require("xlsx");
var querystring = require('querystring');
var axios = require('axios');

var CANDIDATE_URL = 'https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/findAddressCandidates';
var FEATURES_URL = 'https://gis.shelbycountytn.gov/arcgis/rest/services/Election/BallotSplits/MapServer/dynamicLayer/query';

var DistrictParser = {
  readFile(filename, sheetname){
    var workbook = XLSX.readFile(filename);
    var sheet = workbook.Sheets[sheetname];
    return XLSX.utils.sheet_to_json(sheet);
  },
  createNewData(data){
    var promises = [];
    var newData = [];
    data.forEach((item) => {
      var address = item['STREET ADDRESS'];
      var city = item['CITY'];
      var state = item['STATE'];
      var zip = item['ZIP CODE'];
      var acsz = `${address}, ${city}, ${state} ${zip} USA`;
      promises.push(this.fetchDistrict(acsz).then((district) => {
        if(district != 3){
          console.log(`${district} - ${address}`);
        }
        newData.push({
          'STREET ADDRESS': address,
          'CITY': city,
          'STATE': state,
          'ZIP CODE': zip,
          'DISTRICT': district
        });
      }).catch((e) => {
        newData.push({
          'STREET ADDRESS': address,
          'CITY': city,
          'STATE': state,
          'ZIP CODE': zip,
          'DISTRICT': e
        });
      }));
    });
    return Promise.all(promises).then(() => newData).catch((e) => {
      console.error(e);
    });
  },
  createExtents(data){
    var promises = [];
    var newData = [];
    data.forEach((item) => {
      var address = item['STREET ADDRESS'];
      var city = item['CITY'];
      var state = item['STATE'];
      var zip = item['ZIP CODE'];
      var acsz = `${address}, ${city}, ${state} ${zip} USA`;
      promises.push(this.fetchExtent(acsz).then((extent) => {
        newData.push({
          acsz,
          extent
        });
      }).catch((e) => {
        console.error(e);
      }));
    });
    return Promise.all(promises).then(() => newData).catch((e) => {
      console.error(e);
    });
  },
  createFeatures(data){
    var promises = [];
    var newData = [];
    data.forEach((extent) => {
      promises.push(this.fetchFeatures(extent).then((attrs) => {
        newData.push(attrs);
      }).catch((e) => {
        console.error(e);
      }));
    });
    return Promise.all(promises).then(() => newData).catch((e) => {
      console.error(e);
    });
  },
  getDistrict(data){
    var promises = [];
    var newData = [];
    data.forEach((extent) => {
      promises.push(this.fetchFeatures(extent).then((attrs) => {
        newData.push(attrs.MemCityCou);
      }).catch((e) => {
        console.error(e);
      }));
    });
    return Promise.all(promises).then(() => newData).catch((e) => {
      console.error(e);
    });
  },
  fetchDistrict(acsz){
    return new Promise((resolve, reject) => {
      axios.get(`${CANDIDATE_URL}?${this._makeCandidateQuery(acsz)}`).then((response) => {
        var candidates = response.data.candidates;
        if(candidates.length > 0){
          var extent = candidates[0].extent;
          axios.get(`${FEATURES_URL}?${this._makeFeaturesQuery(extent)}`).then((resp) => {
            var attrs = resp.data.features[0].attributes;
            if(attrs.MemCityCou){
              resolve(attrs.MemCityCou);
            } else {
              reject('Memphis City Council not found in results.');
            }
          });
        } else {
          reject('No address candidates found!');
        }
      });
    });
  },
  fetchExtent(acsz){
    return new Promise((resolve, reject) => {
      axios.get(`${CANDIDATE_URL}?${this._makeCandidateQuery(acsz)}`).then((response) => {
        var candidates = response.data.candidates;
        if(candidates.length > 0){
          var extent = candidates[0].extent;
          console.log(candidates[0].score ? candidates[0].score : candidates.length);
          resolve(extent);
        } else {
          reject('No address candidates found!');
        }
      });
    });
  },
  fetchFeatures(extent){
    return new Promise((resolve, reject) => {
      axios.get(`${FEATURES_URL}?${this._makeFeaturesQuery(extent.extent)}`).then((resp) => {
        var attrs = resp.data.features[0] ? resp.data.features[0].attributes : {};
        if(attrs){
          attrs['FULL ADDRESS'] = extent.acsz;
          resolve(attrs);
        } else {
          reject('Memphis City Council not found in results.');
        }
      });
    });
  },
  makeNewWorkbook(data, sheetname){
    var nws = XLSX.utils.json_to_sheet(data);
    var nwbk = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(nwbk, nws, sheetname);
    return nwbk;
  },
  _makeCandidateQuery(address){
    return querystring.stringify({
      SingleLine: address,
      f: 'json',
      outSR: '{"wkid":102100}',
      outFields: '*',
      maxLocations: 4,
      // magicKey: 'dHA9MCNsb2M9MTM3MjM4MiNsbmc9MzMjaG49NjE3NCNsYnM9MTA5OjQzODE1NjA3Ozk6MTkzMDg2ODY7MjoyOTEwNzIwNzsxMDoxNTQyMzcz',
      // distance: 50000,
      // location: '{"x":-10023210.29061107,"y":4184024.6078184936,"spatialReference":{"wkid":102100}}',
    });
  },
  _makeFeaturesQuery(extent){
    return querystring.stringify({
      f: 'json',
      returnGeometry: true,
      spatialRel: 'esriSpatialRelIntersects',
      geometry: JSON.stringify(extent),
      geometryType: 'esriGeometryEnvelope',
      inSR: 102100,
      outFields: '*',
      outSR: 102100,
      layer: '{"source":{"type":"mapLayer","mapLayerId":0}}',
      // outFields: 'Precincts,Splits,MemCityCou,MemSupDis,SCCommDis,SCCongDis,SCSchBDis,TNHouseDis,TNSenDis,Lat,Long',
    });
  },
};

module.exports = DistrictParser;