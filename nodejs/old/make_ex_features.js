#!/usr/local/bin/node

var XLSX = require("xlsx");
const fs = require('fs');
const DistrictParser = require('./DistrictParser.js');
const extents = require('./resources/extents.json');

DistrictParser.createFeatures(extents).then(data => {
  var workbook = DistrictParser.makeNewWorkbook(data, config.sheet);
  XLSX.writeFile(workbook, `${config.path}${config.filename}_features.xlsx`, {
    bookType: "xlsx"
  });
}).catch((e) => {
  console.log(e);
  // exit(0);
});
