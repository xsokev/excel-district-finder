#!/usr/local/bin/node

var XLSX = require("xlsx");
const fs = require('fs');
const DistrictParser = require('../lib/DistrictParser');
const config = require('../config');

console.log("******** START TIME: "+(new Date()).toLocaleTimeString());
var data = DistrictParser.readFile(`${config.excel.path}${config.excel.filename}.xlsx`, config.excel.sheet);
var chunk_count = Math.ceil(data.length / config.max_requests);
var chunks = [];
for(var i = 0; i < chunk_count; i++){
  var start = i * config.max_requests;
  var end = start + config.max_requests;
  var chunk = data.slice(start, end);
  chunks.push(chunk);
}
var next = 0;
var newData = [];
function getData(){
  if(next < chunks.length){
    var chunkData = chunks[next];
    DistrictParser.createNewData(chunkData).then(data => {
      data.forEach((item) => {
        newData.push(item);
      });
      console.log('retrieved batch ' + (next + 1) + ' of ' + chunks.length);
      setTimeout(function(){
        next++;
        getData();
      }, config.request_delay);
    }).catch((e) => {
      exit(0);
    });  
  } else {
    console.log('data is done!');
    var workbook = DistrictParser.makeNewWorkbook(newData, config.sheet);
    XLSX.writeFile(workbook, `${config.path}${config.filename}_edited.xlsx`, {
      bookType: "xlsx"
    });
    console.log('all done!');
    console.log("******** END TIME: "+(new Date()).toLocaleTimeString());
  }
}
getData();