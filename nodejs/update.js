#!/usr/local/bin/node

const prompts = require('prompts');
const sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database(':memory:');
 
(async () => {
  const response = await prompts([
    {
      type: 'number',
      name: 'zip',
      message: 'Zip Code?',
      // validate: value => value < 18 ? `Nightclub is 18+ only` : true
    },
    {
      type: 'confirm',
      name: 'extents',
      message: 'Extents Only?',
      initial: false
      // validate: value => value < 18 ? `Nightclub is 18+ only` : true
    }
  ]);
   
  console.log(response);
})();
