import Vue from 'vue'
import App from './App.vue'

import 'uikit'
import 'uikit/dist/js/uikit-icons'
// import 'uikit/src/js/icons'

import 'uikit/dist/css/uikit.css'

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
