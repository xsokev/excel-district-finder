<?php
  header("Access-Control-Allow-Origin: *");

  $action = $_GET['action'];
  if($action == 'zips'){
    getZips();
  } elseif($action == 'zipdistricts') {
    getZipDistricts();
  } elseif($action == 'zipaddresses') {
    getZipAddresses();
  } elseif($action == 'csv') {
    makeCSV();
  } else {
    die('No action provided!');
  }

  function getZips(){
    $db = _getDB();
    $res = $db->query("SELECT distinct substr(zip,0,6) as zip FROM output_data WHERE council_district is not NULL");
    $data = [];
    foreach($res as $row)
    {
        $data[] = intval($row['zip']);
    }
    sort($data);
    print_r(json_encode($data));
    $db = null;
  }

  function getZipDistricts(){
    $db = _getDB();
    $zip = isset($_GET['zip']) ? $_GET['zip'] : 0;
    $res = $db->query("SELECT DISTINCT voting_location, council_district FROM output_data WHERE zip LIKE '$zip%'");
    $data = [];
    foreach($res as $row)
    {
        if(isset($row['voting_location'])){
          $data[] = [
            "location" => $row['voting_location'],
            "district" => intval($row['council_district']),
          ];
        }
    }
    print_r(json_encode($data));
    $db = null;
  }

  function getZipAddresses(){
    $db = _getDB();
    $zip = isset($_GET['zip']) ? $_GET['zip'] : 0;
    $res = $db->query("SELECT * FROM output_data WHERE zip LIKE '$zip%'");
    $data = [];
    foreach($res as $row)
    {
        $data[] = [
          "address" => $row['address'],
          "city" => $row['city'],
          "location" => $row['voting_location'],
          "district" => intval($row['council_district']),
        ];
    }
    print_r(json_encode($data));
    $db = null;
  }

  function makeCSV(){
    header('Content-Type: text/csv; charset=utf-8');
    $output = fopen('php://output', 'w');

    $db = _getDB();
    $type = isset($_GET['t']) ? $_GET['t'] : 0;
    if($type == 0){
      $zip = isset($_GET['zip']) ? $_GET['zip'] : 0;
      if($zip != 0){
        header('Content-Disposition: attachment; filename=Zip '.$zip.'.csv');
        $dist = isset($_GET['district']) ? $_GET['district'] : 0;
        if($dist != 0){
          $outside = isset($_GET['outside']) ? true : false;
          if($outside){
            $res = $db->query("SELECT * FROM output_data WHERE zip LIKE '$zip%' AND council_district != $dist");
          } else {
            $res = $db->query("SELECT * FROM output_data WHERE zip LIKE '$zip%' AND council_district = $dist");
          }
        } else {
          $res = $db->query("SELECT * FROM output_data WHERE zip LIKE '$zip%'");
        }
      }
    } else {
      $dist = isset($_GET['dist']) ? $_GET['dist'] : 0;
      if($dist != 0){
        header('Content-Disposition: attachment; filename=District '.$dist.'.csv');
        $res = $db->query("SELECT * FROM output_data WHERE council_district = $dist");
      }
    }
    if(isset($res)){
      fputcsv($output, array('STREET ADDRESS','CITY','STATE','ZIP CODE','VOTING LOCATION','COUNCIL DISTRICT','SUPPLEMENT DISTRICT'));
      foreach($res as $row)
      {
        $data = array(
          $row['address'],
          $row['city'],
          $row['state'],
          intval($row['zip']),
          $row['voting_location'],
          $row['council_district'],
          intval($row['sup_district']),
        );
        fputcsv($output, $data);
      }
    } else {
      header('Content-Disposition: attachment; filename=blank.csv');      
      fputcsv($output, array('STREET ADDRESS','CITY','STATE','ZIP CODE','VOTING LOCATION','COUNCIL DISTRICT','SUPPLEMENT DISTRICT'));
    }
    $db = null;
  }

  function _getDB(){
    return new PDO('sqlite:memphisdb.sqlite');
  }
?>