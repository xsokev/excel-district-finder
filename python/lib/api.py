import json
from urllib.parse import urlencode
from urllib3 import PoolManager, disable_warnings
from config import EXTENTS_URL, EXTENTS_QUERY, ELECTION_URL, ELECTION_QUERY

def fetchExtent(address):
  _qs = '?SingleLine=' + address + '&' + urlencode(EXTENTS_QUERY)
  _headers = {'Accept': 'application/json', 'Content-Type': 'application/json; charset=UTF-8'}
  _url = EXTENTS_URL + _qs
  # TODO: fix the SSL error
  http = PoolManager(cert_reqs='CERT_NONE')
  disable_warnings()


  r = http.request('GET', _url, headers=_headers)
  return parseExtent(r.data.decode('utf-8'))

def parseExtent(j):
  candidate = {}
  _data = json.loads(j)
  try:
    _candidates = _data.get('candidates')
    if len(_candidates) == 1:
      candidate = _candidates[0]
    else:
      candidate = _candidates[0]
      for _candidate in _candidates:
        _scoreA = int(candidate.get('score'));
        _scoreB = int(_candidate.get('score'));
        if(_scoreB > _scoreA):
          candidate = _candidate
  except TypeError:
    candidate = None
  
  if candidate is None:
    return None
  else:
    return candidate.get('extent')

def fetchElectionData(extent):
  _qs = '?geometry=' + extent + '&' + urlencode(ELECTION_QUERY)
  _headers = {'Accept': 'application/json', 'Content-Type': 'application/json; charset=UTF-8'}
  _url = ELECTION_URL + _qs
  # TODO: fix the SSL error
  http = PoolManager(cert_reqs='CERT_NONE')
  disable_warnings()

  r = http.request('GET', _url, headers=_headers)
  return parseElectionData(r.data.decode('utf-8'))

def parseElectionData(j):
  electionData = {}
  _data = json.loads(j)
  try:
    _features = _data.get('features')
    if len(_features) > 0:
      _feature = _features[0]
      _attributes = _feature.get('attributes')
      if len(_attributes) > 0:
        for _key in _attributes.keys():
          if _key == 'MemCityCou':
            electionData['council_district'] = _attributes.get(_key)
          elif _key == 'MemSupDis':
            electionData['sup_district'] = _attributes.get(_key)
          elif _key == 'VotingLocationName':
            electionData['voting_location'] = _attributes.get(_key)
  except TypeError:
    electionData = {}

  return electionData
