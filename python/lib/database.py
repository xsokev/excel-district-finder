import sqlite3
from config import DATABASE_PATH
from helpers import validArgs

conn = sqlite3.connect(DATABASE_PATH)

def closeDb():
	conn.close()

def zips():
	_zips = []
	_cursor = conn.execute("SELECT * FROM zips")
	for row in _cursor:
		_zips.append(int(row[0]))
	return _zips

def unverifedExtents(zipcode):
	_data = []
	_query = "SELECT id, printf('%s, %s, %s %s', address, city, state, zip) AS address FROM unverified_extents WHERE zip LIKE '" + str(zipcode) + "%'"
	_cursor = conn.execute(_query)
	for row in _cursor:
		_data.append({
			'id': row[0],
			'address': row[1]
		})
	return _data

def unverifedElectionData(zipcode):
	_data = []
	_query = "SELECT address_id, extent_id, printf('{\"xmin\": %s, \"xmax\": %s, \"ymin\": %s, \"ymax\": %s, \"spatialReference\":{\"wkid\":102100}}', xmin, xmax, ymin, ymax) AS extent FROM unverified_elections WHERE zip LIKE '" + str(zipcode) + "%'"
	_cursor = conn.execute(_query)
	for row in _cursor:
		_data.append({
			'addressId': row[0],
			'extentId': row[1],
			'extent': row[2],
		})
	return _data

def insertExtent(address_id, extent):
	if validArgs(address_id, extent):
		xmin = extent.get('xmin')
		xmax = extent.get('xmax')
		ymin = extent.get('ymin')
		ymax = extent.get('ymax')
		if xmin is not None and xmax is not None and ymin is not None and ymax is not None:
			c = conn.cursor()
			query = "INSERT INTO extents (`address_id`, `xmin`, `xmax`, `ymin`, `ymax`) VALUES ({0}, {1}, {2}, {3}, {4})".format(address_id, xmin, xmax, ymin, ymax)
			c.execute(query)
			conn.commit()
			return True
		else:
			return False

def insertElectionData(extent_id, features):
	if validArgs(extent_id, features):
		sup_district = features.get('sup_district')
		voting_location = features.get('voting_location')
		council_district = features.get('council_district')
		if validArgs(sup_district, voting_location, council_district):
			c = conn.cursor()
			query = "INSERT INTO election (`extent_id`, `voting_location`, `council_district`, `sup_district`) VALUES ({0}, '{1}', {2}, {3})".format(extent_id, voting_location, council_district, sup_district)
			c.execute(query)
			conn.commit()
			return True
		else:
			return False

def getOutputData(zip_code):
	_data = []
	_query = "SELECT * FROM output_data WHERE zip LIKE '" + str(zip_code) + "%'"
	_cursor = conn.execute(_query)
	for row in _cursor:
		_data.append({
			'Street Address': row[0],
			'City': row[1],
			'State': row[2],
			'Zip Code': row[3],
			'Voting Location': row[4],
			'City Council District': row[5],
			'Supplement District': row[6],
		})
	return _data
