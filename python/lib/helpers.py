from datetime import datetime
from time import time

def timestamp():
  print(datetime.fromtimestamp(time()).strftime('%H:%M:%S'))

def log(msg):
  print(msg)

def validArgs(*args):
  valid = True
  for arg in args:
    if arg is None:
      valid = False
  return valid
