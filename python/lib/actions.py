from database import unverifedExtents, insertExtent, unverifedElectionData, insertElectionData, getOutputData
from api import fetchExtent, fetchElectionData
import csv
from helpers import timestamp, validArgs

def getExtents(zip_code):
  timestamp()  
  _data = unverifedExtents(zip_code)
  _total = len(_data)
  print('Total addresses to process: {0}'.format(_total))
  for i in range(_total):
    _item = _data[i]
    extent = fetchExtent(_item.get('address'))
    success = insertExtent(_item.get('id'), extent)
    if success == True:
      print('...added {0} of {1}'.format(i+1, _total))

  timestamp()

def getElectionData(zip_code):
  timestamp()
  _data = unverifedElectionData(zip_code)
  _total = len(_data)
  print('Total extents to process: {0}'.format(_total))
  for i in range(_total):
    _item = _data[i]
    _electionData = fetchElectionData(_item.get('extent'))
    success = insertElectionData(_item.get('extentId'), _electionData)
    if success == True:
      print('...added {0} of {1}'.format(i+1, _total))

  timestamp()

def makeCSV(zip_code, outdir):
  timestamp()
  _data = getOutputData(zip_code)
  _total = len(_data)
  print('Total addresses to convert: {0}'.format(_total))
  with open('{0}{1}.csv'.format(outdir, zip_code), 'w') as csv_file:
    writer = csv.writer(csv_file)
    writer.writerow(['Street Address', 'City', 'State', 'Zip Code', 'Voting Location', 'City Council District', 'Supplement District'])
    for i in range(_total):
      _item = _data[i]
      _addr = _item.get('Street Address')
      _city = _item.get('City')
      _stat = _item.get('State')
      _zipc = _item.get('Zip Code')
      _volo = _item.get('Voting Location')
      _mccd = _item.get('City Council District')
      _supd = _item.get('Supplement District')
  
      if validArgs(_addr, _city, _stat, _zipc, _volo, _mccd, _supd):
        writer.writerow([_addr, _city, _stat, _zipc, _volo, _mccd, _supd])
  
    csv_file.close()

  timestamp()
