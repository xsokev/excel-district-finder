#!/usr/local/bin/python3

import sys
sys.path.insert(0, "./lib/")

import argparse
from database import zips, closeDb
from actions import getExtents, getElectionData, makeCSV

parser = argparse.ArgumentParser(description='This script finds extents for a given address and election data for those extents. It also generates a Spreadsheet with all data for a specified zip code.')
parser.add_argument('-type', default='x')
parser.add_argument('-zip', type=int)
parser.add_argument('-dir')

opts = parser.parse_args()
outdir = './'
if opts.dir is not None:
  outdir = opts.dir
if opts.zip == None:
  sys.exit('A zip code is required!')
else:
  _zips = zips()
  try:
    _zips.index(int(opts.zip))
  except ValueError:
    sys.exit('A valid zip code is required')

if opts.type == 'x' or opts.type == 'ext' or opts.type == 'extents':
  getExtents(opts.zip)
  closeDb()
elif opts.type == 'f' or opts.type == 'districts' or opts.type == 'features':
  getElectionData(opts.zip)
  closeDb()
elif opts.type == 'o' or opts.type == 'out' or opts.type == 'output' or opts.type == 'csv':
  makeCSV(opts.zip, outdir)
  closeDb()
elif opts.type == 'a' or opts.type == 'all':
  try:
    confirm = input('It is recommended that you execute the commands separately. This operation could take a long time. Are you sure you want to proceed? (Y/n): ')
  except SyntaxError:
    confirm = None

  if confirm == 'y' or confirm == 'Y':
    getExtents(opts.zip)
    getElectionData(opts.zip)
    makeCSV(opts.zip, outdir)
    closeDb()
  else:	
    sys.exit('Operation aborted!')
else:
  sys.exit('Invalid execution option! Valid options are: [extents, features, csv, or all]')