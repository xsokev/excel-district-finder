#!/usr/local/bin/python3

import sys
sys.path.insert(0, "./lib/")

from database import zips, closeDb
from actions import getExtents, getElectionData, makeCSV

zips = [
  38002,
  38014,
  38016,
  38017,
  38018,
  38019,
  38028,
  38053,
  38101,
  38103,
  38104,
  38105,
  38106,
  38107,
  38108,
  38109,
  38111,
  38112,
  38113,
  38114,
  38115,
  38116,
  38117,
  38118,
  38119,
  38120,
  38122,
  38125,
  38126,
  38127,
  38128,
  38131,
  38132,
  38133,
  38134,
  38135,
  38137,
  38138,
  38139,
  38141,
  38152,
  38157,
  38183,
]
for zip in zips:
  getExtents(zip)
  getElectionData(zip)
  makeCSV(zip, '../exports/')
closeDb()
